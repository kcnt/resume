import Vue from 'vue'
import VueFlux from 'vue-flux'

export default () => {
  Vue.use(VueFlux)
}
