export const MakeSlug = name => {
  return name.toLowerCase().replace(/ /g, '-')
}
