export const FileLoader = (fs, name) => {
  const names = `${name}s`
  const _res = fs.readdirSync(`static/json/${names}/en`)
  return _res.reduce((p, filename) => {
    if (!p[names]) {
      p[names] = []
    }

    p[names].push({
      ...JSON.parse(
        fs.readFileSync(`static/json/${names}/en/${filename}`, 'utf8')
      ),
      __file: filename.replace('.json', '')
    })
    return p
  }, {})
}
