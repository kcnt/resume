<a name="unreleased"></a>

## [Unreleased]

### Feature

- **content:** create Works “2019-02-16-maxile-co-ltd” [cms]

### Fixes Bug

- **ui:** add if to avoid page render error in version

### Improving application

- **git-command:** hashs result should contain commit of previous tag

<a name="v0.2.0"></a>

## [v0.2.0] - 2019-02-15

### Feature

- **page:** update contact page and remove mail in index

### Fixes Bug

- **parser:** commit parser when scope is not exist

### Improving application

- **css:** remove all duplicate css to header
- **css:** add scoped css
- **page:** include git history to 50, fix tag not exist
- **version-page:** improve changelog format and algorithm

<a name="v0.1.1"></a>

## [v0.1.1] - 2019-02-14

### Feature

- **all:** merge all new feat to one commit
- **page:** implement new page call version.

### Fixes Bug

- **css:** scss module parser error
- **css:** style break and error
- **css:** css not exist in container
- **css:** css not appied in layout container

### Improving application

- **facebook:** add og:image in facebook

### Improving performance

- **build:** improve build process
- **clean:** remove some config that might slow build time
- **css:** split css in layout out
- **import:** remove duplicate import google font
- **json:** multiple json file should belong on static
- **mode:** reverse to univeral instead of spa
- **ui:** remove scope and change to spa

<a name="v0.1.0"></a>

## v0.1.0 - 2019-02-11

### Feature

- **cms:** json file should be on assets folders
- **cms:** support multiple language and relation tag
- **content:** create Skills “2019-02-10-git” [cms]
- **content:** update Informations “social” [cms]
- **content:** update Informations “profile_thai” [cms]
- **content:** create Tags “infrastructure” [cms]
- **content:** create Tags “design” [cms]
- **content:** create Tags “bitbucket” [cms]
- **content:** create Tags “gitlab” [cms]
- **content:** create Tags “github” [cms]
- **content:** create Tags “git” [cms]
- **content:** create Tags “development” [cms]
- **content:** create Tags “bankend” [cms]
- **content:** create Tags “frontend” [cms]
- **content:** create Skills “2019-02-10-vuejs” [cms]
- **content:** update Informations “profile” [cms]
- **content:** create Interests “2019-02-10-natural-language-processing” [cms]
- **content:** create Interests “2019-02-10-big-data” [cms]
- **content:** update Projects “2018-12-18-portfolio-website” [cms]
- **content:** create Works “2019-02-10-base-playhouse” [cms]
- **content:** create Tags “vuejs” [cms]
- **content:** create Tags “android” [cms]
- **content:** create Tags “ios” [cms]
- **content:** create Tags “mobile” [cms]
- **content:** create Tags “application” [cms]
- **content:** create Tags “website” [cms]
- **content:** create Tags “software” [cms]
- **index:** implement component for index page
- **ui:** implement first index page
- **ui:** implement new ui of index page
- **ui:** implement success page

### Fixes Bug

- **cms:** save to wrong position
- **cms:** highlight should be 1 line string
- **cms:** avoid sharing url path
- **css:** scoped css so bulma is missing in index page
- **css:** remove global css due to overright in admin page

### Improving application

- **cms:** change cms language instead of fix lang
- **cms:** change the way to enter social media link
- **cms:** change meta data in cms
- **cms:** add gallery and fix some error
- **contact:** create contact page to connect with me
- **css:** new transform css
- **css:** make font is a default css in default
- **ui:** custom ui and pages
- **ux:** add back to home success redirect

### Improving performance

- **css:** reduce css files
- **icon:** remove unused fontawesome icon

[unreleased]: /compare/v0.2.0...HEAD
[v0.2.0]: /compare/v0.1.1...v0.2.0
[v0.1.1]: /compare/v0.1.0...v0.1.1
