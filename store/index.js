import { FileLoader } from '~/assets/apis/file.js'

import info from '~/static/json/personal/en/information.json'
import social from '~/static/json/personal/social.json'

export const state = () => ({
  information: {}
})

export const mutations = {
  loadInformation(state, info) {
    state.information = {
      ...state.information,
      ...info
    }
  }
}

export const actions = {
  nuxtServerInit({ commit }) {
    info.website = social.website
    info.socials = social.socials
    commit('loadInformation', info)

    const fs = require('fs')
    const project = FileLoader(fs, 'project')
    commit('loadInformation', project)

    const work = FileLoader(fs, 'work')
    commit('loadInformation', work)

    const skill = FileLoader(fs, 'skill')
    commit('loadInformation', skill)
  }
}
