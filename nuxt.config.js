const fs = require('fs')

const pkg = require('./package')
const GitRevisionPlugin = require('git-revision-webpack-plugin')
const gitRevision = new GitRevisionPlugin({
  branch: true,
  versionCommand: 'describe --tags',
  commithashCommand: 'rev-parse --short=7 HEAD',
  branchCommand:
    'log $(git tag --sort version:refname | tail -n 2 | head -n 1)..$(git describe --tags --abbrev=0) --pretty=format:"%H: %s [%at]"'
})

const env = process.env.NODE_ENV
const url =
  env === 'production' ? 'https://kamontat.net' : 'http://localhost:3000'

module.exports = {
  mode: 'universal', // universal
  modern: false,

  /*
   ** Headers of the page
   */
  head: {
    title: 'Kamontat Chantrachirathumrong',
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description
      },
      {
        hid: 'version',
        name: 'version',
        content: pkg.version
      },
      {
        hid: 'author',
        name: 'author',
        content: pkg.author
      },
      {
        hid: 'og:url',
        property: 'og:url',
        content: url
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content: 'Kamontat Chantrachirathumrong'
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: pkg.description
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: `${url}/images/poster.jpg`
      },
      ...Object.keys(pkg.dependencies).map(name => {
        return {
          name: 'node:dependency',
          content: `${name}:${pkg.dependencies[name]}`
        }
      }),
      ...Object.keys(pkg.devDependencies).map(name => {
        return {
          name: 'node:dependency:dev',
          content: `${name}:${pkg.devDependencies[name]}`
        }
      })
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff'
  },

  env: {
    nodeEnv: env,
    baseUrl:
      env === 'production' ? 'https://kamontat.net' : 'http://localhost:3000',
    version: pkg.version,
    license: pkg.license,
    buildDate: +new Date(),
    pkg: pkg,
    git: {
      version: gitRevision.version(),
      hash: gitRevision.commithash(),
      hashs: gitRevision.branch()
    }
  },

  /*
   ** Global CSS
   */
  css: ['~/assets/styles/importer.scss'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/scrollto.js',
    {
      mode: 'client',
      src: '~/plugins/flux.js'
    }
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/google-analytics',
    'nuxt-fontawesome',
    '@nuxtjs/markdownit'
  ],

  'google-analytics': {
    id: 'UA-124896160-6'
  },

  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  fontawesome: {
    component: 'fa',
    imports: [
      //import whole set
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: ['faArrowCircleLeft', 'faChevronUp', 'faUser', 'faEnvelope']
      },
      {
        set: '@fortawesome/free-brands-svg-icons',
        icons: [
          'faGithub',
          'faGitlab',
          'faTwitter',
          'faFacebook',
          'faLinkedin',
          'faMedium'
        ]
      }
    ]
  },

  // See https://github.com/markdown-it/markdown-it
  markdownit: {
    preset: 'commonmark',
    linkify: true,
    breaks: true,
    injected: true
    // use: [
    //   ['markdown-it-container', containerName],
    //   'markdown-it-attrs'
    // ]
  },

  generate: {
    // subFolders: false,
    fallback: '404.html',
    routes: function(callback) {
      try {
        const response = []

        const works = fs.readdirSync(`static/json/works/en/`)
        // const skills = fs.readdirSync(`static/json/skills/en/`)
        const projects = fs.readdirSync(`static/json/projects/en/`)

        works.forEach(w => response.push(`/work/${w.replace('.json', '')}`))
        // skills.forEach(s => response.push(`/skill/${s.replace('.json', '')}`))
        projects.forEach(p =>
          response.push(`/project/${p.replace('.json', '')}`)
        )

        callback(undefined, response)
      } catch (e) {
        callback(e)
      }
    }
  },

  /*
   ** Build configuration
   */
  build: {
    // cache: true,
    // extractCSS: true,
    parallel: false,
    publicPath: '/_kc/',
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },
    splitChunks: {
      pages: false,
      commons: true,
      layouts: false
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, { isDev, isClient, loaders }) {
      if (isDev) loaders.cssModules.localIdentName = '__kc_[name]_[local]'
      else
        loaders.cssModules.localIdentName =
          '[sha512:contenthash:base64:4]-[sha512:contenthash:hex:4]-[sha256:contenthash:base64:4]-[sha256:contenthash:hex:4]'

      config.node = {
        fs: 'empty'
      }

      const index = config.module.rules.findIndex(v =>
        v.test.toString().includes('scss')
      )
      const scssRule = config.module.rules[index]

      scssRule.oneOf[0].use[1].options.localIdentName =
        loaders.cssModules.localIdentName
      config.module.rules[index] = scssRule

      // Run ESLint on save
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
